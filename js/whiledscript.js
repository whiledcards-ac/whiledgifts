/** 
 * 
 * Please divide this file into sections for different front-end purposes. 
 * ATTENTION: Back-end should be in another JS file!
 */


 /**
  * 
  * SMOOTH SCROLL
  */
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener("click", function(e) {
        e.preventDefault();

    document.querySelector(this.getAttribute("href")).scrollIntoView({
            behavior: "smooth"
        });
    });
});

/**
 * 
 * BUTTON TRIGGERS
 */
var btnOpenRandomizer = document.getElementById('btn-open-randomizer');
var memeteca = document.getElementById('memeteca-modal');
var btnCloseModal = document.getElementById('btn-close-randomizer');

// Open Randomizer
btnOpenRandomizer.addEventListener('click', function() {
    memeteca.classList.add('is-active');
    memegenerator();
})

// Close Meme Randomizer
btnCloseModal.addEventListener('click', function() {
    memeteca.classList.remove('is-active');
})

/**
 * 
 * LOADERS
 */
var masterCodersTiles = document.getElementById('master-coders-tiles');
var cadetsTiles = document.getElementById('code-cadets-tiles');

// Master Coders & Code Cadets Tiles
document.addEventListener("DOMContentLoaded", function(event) {
    mcs.printMasterCoders(masterCodersTiles);
    codeCadets.printCadets(cadetsTiles);
});