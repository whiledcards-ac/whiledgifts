// list of colours from Bulma Framework
var colourList = ['primary', 'link', 'success', 'warning', 'danger', 'grey', 'dark'];
// prefix to background colour from Bulma Framework
const COLOR_BG = 'has-background-';
// prefix to cadet profile picture
const PATH_PICTURE = './assets/img/cadets/';
// tile row (to add two cadets each)
var tileRowOpen = '<div class="tile tileRow">';
var tileRowClose = '</div>';

var codeCadets = (function() {

    var cadets = [
        {
            name: "Alberto",
            message: "Dúvidas? Não, só desabafos.",
            picture: "alberto.jpeg"
        },
        {
            name: "Noqas",
            message: "Muito fixe terem-me ajudado a sentir menos awkward a apresentar cenas, thumbs up!",
            picture: "ana.jpeg"
        },
        { 
            name: "John Wick",
            message: "Foi um ano buéda fixe pessoal!",
            picture: "andre.jpeg"
        },
        {
            name: "Carol",
            message: "A Academia de Código foi a experiência mais brutal da minha vida, em todos os sentidos.",
            picture: "carolina.jpeg"
        },
        {
            name: "Daniel",
            message: "O meu feedback em relação ao bootcamp é simplesmente que, até ao momento em que escrevo isto, tudo aparenta estar a ser cumprido como me foi indicado ao início. Realmente aprendi a programar e aprendi a aprender, novamente a programar. Sinto-me capaz de aprender qualquer linguagem de programação e trabalhar profissionalmente enquanto programador. Para tudo estar bem só falta mesmo assinar o contrato de trabalho e iniciar esta jornada no mundo da programação ;)",
            picture: "daniel.jpeg"
        },
        { 
            name: "Diogo",
            message: "Curtia bué perceber de testes.",
            picture: "diogo.jpeg"
        },
        {
            name: "Felipe L.",
            message: "O início foi um sonho. O meio foi o todo. O fim é só saudades.",
            picture: "felipe.jpeg"
        },
        {
            name: "Filipe F.",
            message: "Damn, what a ride it was! Grateful for having decided to turn one more page of my life, grateful to have chosen <Academia de Código_> has the start of something totally out of the box, to enter in the realms of Programming, grateful to have met all the amazing code cadets colleagues and Master Coders, I’ve learned so much from them! From a totally newbie to a Junior Full Stack Developer, in just 14 weeks, seemed something kinda utopic in the beginning, but the truth is, though difficult and hard working, it is totally worth it in the end. I’m very thrilled and excited for what’s ahead!",
            picture: "filipeF.jpeg"
        },
        {
            name: "Filipe P.",
            message: "Quem inventou o Javascript devia falecer.",
            picture: "filipeP.jpeg"
        },
        {
            name: "Chico",
            message: "A hackathon devia ser ilegal.",
            picture: "francisco.jpeg"
        },
        {
            name: "Guilherme",
            message: "É a última semana e estou a morrer. Dói-me um bocadinho a cabeça. Não consigo dizer mais que isto, obrigado.",
            picture: "guilherme.jpeg"
        },
        {
            name: "Hugo",
            message: "Se um dia o nosso amor fizer sentido estarmos juntos não tem qualquer sentido",
            picture: "hugo.jpeg"
        },
        {
            name: "Israel",
            message: "Getting my Prince Albert hurt a lot less than the bootcamp.",
            picture: "israel.jpeg"
        },
        {
            name: "João",
            message: "A life changing experience, worth all your money, sweat and tears.",
            picture: "joao.jpeg"
        },
        {
            name: "Júlio",
            message: "01100010 01101001 01110000 00100000 01100010 01101111 01110000.",
            picture: "julio.jpeg"
        },
        {
            name: "Leandro",
            message: "Ficar a arder nunca foi tão divertido.",
            picture: "leandro.jpeg"
        },
        {
            name: "NicoLuís",
            message: "Se precisarem de abraços, puppies ou Grandma hit me up. I have the good shit.",
            picture: "luisS.jpeg"
        },
        {
            name: "Luigi",
            message: "Quero aproveitar a oportunidade para vos agradecer a todos, pelos conhecimentos, pelos ensinamentos, pelos exemplos que me transmitiram durante este Bootcamp todo que levo daqui para o futuro. Muito obrigado!",
            picture: "luisR.jpeg"
        },
        {
            name: "Mahmudul",
            message: "What Mariana said.",
            picture: "mahmudul.jpeg"
        },
        {
            name: "Maria Marlene",
            message: "The bootcamp is… like a summer camp that never ends. Thank you for helping me reshaping my career!",
            picture: "maria.jpeg"
        },
        {
            name: "Mariana",
            message: "Mahma... Duuul. Didn't mean to make you cry, if I'm not back this time again tomorrow... Carry on, carry on... 'Cause nothing really matters to Mahmudul.",
            picture: "mariana.jpeg"
        },
        {
            name: "Marta",
            message: "The experience here was beyond my wildest dreams and I'm pretty grateful for the sleepless nights, the mental crashes, the challenges… but, most importantly, for making us always feel that, no matter how many times we wrote a smelly code, you guys loved us anyway! Thank you for making me achieve that place where possible and impossible meet, in order to become the master of the possimpible (like Barney uses to say)!",
            picture: "marta.jpeg"
        },
        {
            name: "Matheus",
            message: "Roberta Xerecaaaaa.",
            picture: "matheus.jpeg"
        },
        {
            name: "Miguel",
            message: "Ama-te.",
            picture: "miguel.jpeg"
        },
        {
            name: "Pedro",
            message: "Antes de mais um muito obrigado, devido ao vosso apoio e incentivo consegui manter a vontade e empenho necessários para conseguir ultrapassar as imensas dificuldades pelas quais passei. Vocês são uns bacanos, keep it up!",
            picture: "pedro.jpeg"
        },
        {
            name: "Rafael",
            message: "それはこれまでの私の人生で最高の経験でした。ありがとうございました！",
            picture: "rafael.jpeg"
        },
        {
            name: "Raquel",
            message: "Podemos dar mais uma iteração neste bootcamp?",
            picture: "raquel.jpeg"
        },
        {
            name: "Ricardo",
            message: "Bem-vindos ao Bootcamp de 14 semanas a perder saúde!",
            picture: "ricardo.jpeg"
        },
        {
            name: "Tiago Santos",
            message: "Olá, sou o Tiago, tenho 22 anos e esta é a primeira vez que estou a fazer esta declaração. É para dizer que NUNCA cheguei atrasado a nenhuma aula e gosto IMENSO do Róbã e de todos os MCs. Muito obrigado.",
            picture: "tiagoS.jpeg"
        },
        {
            name: "Tiago Vale",
            message: "Hoje não temos nada em comum.",
            picture: "tiagoV.jpeg"
        },
        {
            name: "Walter",
            message: "Olá, eu queria agradecer à Christina, à Audrey, ao Rolo... Ah e claro, a Sara também.",
            picture: "walter.jpeg"
        },
        { 
            name: "Adrian",
            message: "It's better to fade away than to burn out.",
            picture: "adrian.jpeg"
        }
    ];

    return {

        printCadets : function(parentDiv) {

            var tileCounter = 1;
            var reverse = false;
            
            var allCodeCadets = cadets.reduce((acc, cadet) => {

                var str;
                var tile;
                var colour = colourList[Math.round(Math.random() * colourList.length)];

                var tilePicRight = `
                <!-- Tile Right -->
                <div class="tile is-parent is-vertical has-effect-resize-1">
                    <article class="tile is-child notification is-${colour}">
                        <article class="media">
                            <!-- Testemonial Text -->
                            <div class="media-content is-vertical-centered">
                                <p class="title has-text-right">${cadet.name}</p>
                                <p class="has-text-right" id="modalText">${cadet.message}</p>
                            </div>
                            <!-- Code Cadet Picture -->
                            <figure class="media-right image is-128x128 is-vertical-centered">
                                <img class="is-rounded" src="${PATH_PICTURE}${cadet.picture}">
                            </figure>
                        </article>
                    </article>
                </div>`;

                var tilePicLeft = `
                <!-- Tile Left -->
                <div class="tile is-parent is-vertical has-effect-resize-1">
                    <article class="tile is-child notification is-${colour}">
                        <article class="media">
                            <!-- Code Cadet Picture -->
                            <figure class="media-left image is-128x128 is-vertical-centered">
                                <img class="is-rounded" src="${PATH_PICTURE}${cadet.picture}">
                            </figure>
                                <!-- Testemonial Text -->
                                <div class="media-content is-vertical-centered">
                                    <p class="title">${cadet.name}</p>
                                    <p id="modalText">${cadet.message}</p>
                                </div>
                        </article>
                    </article>
                </div>`;
        
                if (reverse === false) {
                    tile = tilePicRight;
                } else {
                    tile = tilePicLeft;
                }

                if (tileCounter % 2 !== 0) {
                    str = tileRowOpen.concat(tile);
                    reverse = !reverse;
                } else {                    
                    str = tile.concat(tileRowClose);

                }
                
                tileCounter++; 
                return acc.concat(str);

            },'');

            parentDiv.innerHTML = allCodeCadets;

        }
    }

}());

