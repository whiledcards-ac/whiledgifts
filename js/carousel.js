const PATH_NICO_MEMES = 'http://zentrynetwork.website/ac/nicocadets/';
// const PATH_NICO_MEMES = './assets/nicocadets/';

var pics = [
    'meme_alberto.png',
    'meme_ana.png',
    'meme_andre.png',
    'meme_carol.png',
    'meme_daniel.png',
    'meme_diogo.png',
    'meme_felipe.png',
    'meme_felipeLisboa.png',
    'meme_filipe_roshi.png',
    'meme_francisco.png',
    'meme_guilherme.png',
    'meme_hugo.png',
    'meme_israel.png',
    'meme_joao.jpeg',
    'meme_julio.png',
    'meme_leandro.png',
    'meme_luigi.png',
    'meme_luis.png',
    'meme_mahmudul.png',
    'meme_maria.png',
    'meme_mariana.png',
    'meme_marta.png',
    'meme_matheus.png',
    'meme_miguel.png',
    'meme_pedro.png',
    'meme_rafael.png',
    'meme_raquel.png',
    'meme_ricardo.png',
    'meme_tiagoS.png',
    'meme_tiagoValle.png',
    'meme_walter.png',
    'meme_adrian.gif'
]

var idx = 0;
var path;

var btnNext = document.getElementById('btn-next');
var btnPrevious = document.getElementById('btn-previous');

document.getElementById('nico-meme').setAttribute('src', PATH_NICO_MEMES + pics[idx]);

function nextPic() {

    if (idx === pics.length - 1) {
        idx = 0;
    } else {
        idx++;
    }

    path = PATH_NICO_MEMES + pics[idx];
    document.getElementById('nico-meme').setAttribute('src', path);

}

function previousPic() {

    if (idx === 0) {
        idx = pics.length - 1;
    } else {
        idx--;
    }

    path = PATH_NICO_MEMES + pics[idx];
    document.getElementById('nico-meme').setAttribute('src', path);

}

btnNext.addEventListener('click', nextPic, true);
btnPrevious.addEventListener('click', previousPic, true);