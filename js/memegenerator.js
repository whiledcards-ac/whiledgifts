// array to store memes numbers
var memeArray = [];

// Total amount of memes to be selected (must be the same amount of files on assets/memes folder)
var totalMemes = 99;

// Meme Number Placeholder on Tile
var memeNumber = document.getElementById('meme-number');

// populate the array with meme numbers
for (let i = 1; i <= totalMemes; i++) {
    memeArray.push(i);
}

// Button Get Meme
document.getElementById('btn-get-meme').addEventListener('click', memegenerator, true);

// meme generator function
function memegenerator() {

    var randomMeme = memeArray[Math.round(Math.random() * memeArray.length)];

    var path  = 'http://zentrynetwork.website/ac/memes/' + randomMeme + '.gif';

    document.getElementById("meme").setAttribute("src", path );
    memeNumber.textContent = randomMeme;

}

// HTML total amount of memes loader
document.getElementById('totalMemes').textContent = totalMemes;


// object.onload = function