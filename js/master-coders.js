var mcs = (function() {

    // Standard path to all MCs pictures
    const SRC_IMG_MC = './assets/img/mcs/';

    // Master Coders data
    var masterCoders = [
        {
            name: 'Rolo',
            image: 'rolo.png',
            description: "Master of GIT, pioneer amongst the dutches and hula dancer. Wanna hear a joke? Don't."
        },
        {
            name: 'Christina',
            image: 'christina.png',
            description: "Unable to understand Robin? She'll draw it for you. Never. Kick. Her. Out. Of. AppleTV."
        },
        {
            name: 'Audrey',
            image: 'audrey.png',
            description: "Hail Audrey, full of smarts, the clean code is with thee. Blessed art thou amongst cadets, and blessed is the fruit of thy Apple, The Code. Holy Audrey, Mother of Null Pointers, pray for us shitty coders, now and at the hour of our hackathon. Socket!"
        },
        {
            name: 'Sara',
            image: 'sara.png',
            description: "Siiiiim, Sara! Those were the first words we've learnt from you. Those are the words we will carry in our null pointers' hearts."
        },
        {
            name: 'Robin',
            image: 'robin.png',
            description: "Always look at the right side of life, não é?"
        },
    ];

    return {

        /**
         * Prints a new tile for each master coder and insert it into 'master-coders-tiles' div
         */
        printMasterCoders : function(parentDiv) {

            /**
             * Iterate over the master coders data, creating a new tile for each of them
             */
            var allMasterCoders = masterCoders.reduce((str, mc) => {

                var mcTile = 
                `
                <div class="tile ml-1 mr-1 mt-1 mb-1">
                    <div class="tile is-child box is-shadowless">
                        <!-- Name -->
                        <p class="title has-text-centered has-text-info">${mc.name}</p>
                        <!-- Picture -->
                        <figure class="image is-128x128 mx mb-3 has-effect-resize-2">
                            <img class="is-rounded" src="${SRC_IMG_MC}${mc.image}">
                        </figure>
                        <!-- Description -->
                        <p class="mt-3">${mc.description}</p>
                    </div>
                </div>
                `;

                /**
                 * Concatenate the tile to the whole string to be added
                 */
                return str.concat(mcTile);

            }, '');

            /**
             * Add the whole string to 'master-coders-tiles' div 
             */
            parentDiv.innerHTML = allMasterCoders;
        }
    }
}());